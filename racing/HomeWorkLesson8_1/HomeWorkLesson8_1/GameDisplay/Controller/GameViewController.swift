import UIKit
import Rswift

//@IBDesignable
class GameViewController: UIViewController {
    
    var scoreText: String?{
        didSet {
            self.labelScore.font = UIFont(name: "Korataki-LightItalic", size: 17)
            self.labelScore.text = self.scoreText
        }
    }
    
    var block = UIImageView()
    var car = UIImageView()
    var itersectionTimer = Timer()
    var animateLinesTimer = Timer()
    var animateRoadsideTimer = Timer()
    var animateBlocksTimer : Timer?
    var step: CGFloat = 0.0
    var width: CGFloat = 0.0
    var playerConfig = Config()
    var players = [Settings]()
    var currentPlayer = Settings()
    var dateLastPlay = String()
    var scoores = 0
    var arrayOfTrees = [UIImage]()
    var animateTimeInterval = 0.0
    var stopAnimations = false
    var isCrashed = false
    var isCarOut = false
    
    @IBOutlet weak var leftRoadsideView: UIView!
    @IBOutlet weak var rightRoadsideView: UIView!
    @IBOutlet weak var roadMainView: UIView!
    @IBOutlet weak var leftRoadsideImageView: UIImageView!
    @IBOutlet weak var rightRoadsideImageView: UIImageView!
    @IBOutlet weak var labelScore: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let data = StorageManager.shared.loadPlayersSettings(), let player = data.last {
            self.currentPlayer = player
        } else {
            self.currentPlayer.loadDefaultsSettings()
        }
        playerConfig.loadStartSettings(player: self.currentPlayer)
        self.setupRoadsideElements()
        self.setupGesture()
        self.scoreText = "Scoores: 0"
        self.step = self.roadMainView.bounds.width / 4
        self.width = self.leftRoadsideView.frame.width
        self.animateTimeInterval = Double(((self.width * 2) + self.view.frame.height)) / Double(self.playerConfig.speed)
        self.car = self.createCar()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.animateLines()
        self.animateBlocks()
        self.animateRoadside()
    }
    
    @IBAction func swipeRight(_ sender: UISwipeGestureRecognizer) {
        moveRight()
    }
    
    @IBAction func swipeLeft(_ sender: UISwipeGestureRecognizer) {
        moveLeft()
    }
    
    @IBAction func jumpCar(_ sender: UITapGestureRecognizer) {
        jumpCar()
    }
    
    @IBAction func goMainMenu(_ sender: UITapGestureRecognizer) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK: - Setup enviroment
    
    func setupGesture() {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(swipeRight(_:)))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(swipeLeft(_:)))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        let tapJump = UITapGestureRecognizer(target: self, action: #selector(jumpCar(_:)))
        tapJump.numberOfTapsRequired = 1
        self.roadMainView.addGestureRecognizer(tapJump)
    }
    
    func setupRoadsideElements() {
        for item in Constants.roadsideIcons {
            if let icon = Manager.shared.getIconFor(item) {
                arrayOfTrees.append(icon)
            }
        }
    }
    
    //MARK: - Creating environment
    
    func createCar() -> UIImageView {
        playerConfig.car.frame = CGRect(x: self.roadMainView.bounds.width / 2 + 1, y: self.roadMainView.bounds.height - self.step * 2, width: self.step - 2, height: self.step * 1.5)
        playerConfig.car.contentMode = .scaleAspectFill
        self.roadMainView.addSubview(playerConfig.car)
        return playerConfig.car
    }
    
    func createLines() -> UIView {
        let lines = UIView()
        lines.frame = CGRect(x: (self.roadMainView.bounds.width / 2) - 5, y: -self.width, width: 10, height: self.width)
        lines.backgroundColor = .white
        self.roadMainView.insertSubview(lines, belowSubview: self.car)
        return lines
    }
    
    func createRoadsideElement(originX: CGFloat) -> UIImageView {
        let roadsideElement = UIImageView()
        roadsideElement.frame = CGRect(x: originX, y: -self.width, width: self.width, height: self.width)
        roadsideElement.image = arrayOfTrees.randomElement()
        roadsideElement.contentMode = .scaleAspectFill
        return roadsideElement
    }
    
    func randomPositionObstacles() -> UIImageView {
        let position = Enums.PositionObstacles.allCases.randomElement()
        switch position {
            case .firstPosition :
                playerConfig.block.frame = CGRect(x: self.roadMainView.bounds.width/2 - (2 * self.step) + 1, y: -self.step, width: self.step - 2, height: self.step - 2)
            case .secondPosition :
                playerConfig.block.frame = CGRect(x: self.roadMainView.bounds.width/2 - self.step + 1, y: -self.step, width: self.step - 2 , height: self.step - 2)
            case .thirdPosition :
                playerConfig.block.frame = CGRect(x: self.roadMainView.bounds.width/2 + self.step + 1, y: -self.step, width: self.step - 2, height: self.step - 2)
            case .fourthPosition :
                playerConfig.block.frame = CGRect(x: self.roadMainView.bounds.width/2 + 1, y: -self.step, width: self.step - 2, height: self.step - 2)
            case .none:
                break
        }
        playerConfig.block.contentMode = .scaleAspectFit
        self.roadMainView.insertSubview(playerConfig.block, belowSubview: self.car)
        return playerConfig.block
    }
    
    func createGameOverScreen() {
        self.savePlayerResult()
        let gameOverScreen = UIView()
        let gameOverLabel = UILabel()
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(goMainMenu(_:)))
        gameOverScreen.addGestureRecognizer(tapRecognizer)
        gameOverLabel.font = UIFont(name: "Korataki-LightItalic", size: 30)
        UIView.animate(withDuration: 0.2) {
            gameOverScreen.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            gameOverScreen.backgroundColor = .black
            gameOverLabel.backgroundColor = .black
            gameOverLabel.numberOfLines = 3
            gameOverLabel.textAlignment = .center
            gameOverLabel.textColor = .red
            gameOverLabel.frame = CGRect(x: self.view.frame.width / 6, y: 2 * self.view.frame.height / 5, width: 2 * self.view.frame.width / 3, height: self.view.frame.height / 5)
            gameOverLabel.text = """
            Game Over
            Your scores
            \(self.scoores)
            """
            self.view.addSubview(gameOverScreen)
            gameOverScreen.addSubview(gameOverLabel)
        }
    }
    
    //MARK: - Saving results
    
    func saveDateLastPlay() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy hh:mm"
        let date = Date()
        self.currentPlayer.lastPlayDate = dateFormatter.string(from: date)
    }
    
    func savePlayerResult() {
        if let data = StorageManager.shared.loadPlayersSettings() {
            self.players = data
        }
        self.saveDateLastPlay()
        self.currentPlayer.scoores = self.scoores
        if self.players.isEmpty {
            self.currentPlayer.isNewPlayer = false
            self.players.append(self.currentPlayer)
        } else {
            if self.currentPlayer.isNewPlayer {
                self.players.removeLast()
                self.currentPlayer.isNewPlayer = false
                self.players.append(self.currentPlayer)
            } else {
                self.players.append(self.currentPlayer)
            }
        }
        StorageManager.shared.savePlayersSettings(players: self.players)
        UserDefaults.standard.synchronize()
    }
    
    //MARK: - Animate environment
    
    func animateLines() {
        self.animateLinesTimer = .scheduledTimer(withTimeInterval: Double(width + 20) / Double(playerConfig.speed), repeats: true) { (_) in
            let whiteLines = self.createLines()
            UIView.animate(withDuration: self.animateTimeInterval, delay: 0, options: .curveLinear, animations: {
                whiteLines.frame.origin.y += self.width * 2 + self.leftRoadsideView.frame.height
            }) { (_) in
                whiteLines.removeFromSuperview()
            }
        }
    }
    
    func animateRoadside() {
        self.animateRoadsideTimer = .scheduledTimer(withTimeInterval: Double(width) / Double(playerConfig.speed), repeats: true) { (_) in
            let leftElement = self.createRoadsideElement(originX: self.leftRoadsideView.bounds.origin.x)
            let rightElement = self.createRoadsideElement(originX: self.rightRoadsideView.bounds.origin.x)
            self.leftRoadsideView.addSubview(leftElement)
            self.rightRoadsideView.addSubview(rightElement)
            UIView.animate(withDuration: self.animateTimeInterval, delay: 0, options: .curveLinear, animations: {
                leftElement.frame.origin.y += self.width * 2 + self.leftRoadsideView.frame.height
                rightElement.frame.origin.y += self.width * 2 + self.rightRoadsideView.frame.height
            }) { (_) in
                leftElement.removeFromSuperview()
                rightElement.removeFromSuperview()
            }
        }
    }
    
    func animateBlocks() {
        self.block = self.randomPositionObstacles()
        if !self.stopAnimations {
            UIView.animate(withDuration: self.animateTimeInterval, delay: 0, options: .curveLinear, animations: {
                self.itersection()
                self.block.frame.origin.y += self.width * 2 + self.leftRoadsideView.frame.height
            }) { (_) in
                if self.isCrashed {
                    self.explosionCar()
                } else {
                    self.block.removeFromSuperview()
                    self.scoores += 1
                    self.scoreText = "Scoores: \(self.scoores)"
                    self.animateBlocks()
                }
            }
        }
    }
    
    //MARK: - Check crash car
    
    func itersection() {
        self.itersectionTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (_) in
            if self.block.layer.presentation()?.frame.intersects(self.car.frame) ?? false {
                self.isCrashed = true
            }
        }
    }
    
    func checkOutRoadside() {
        if (self.car.frame.origin.x < 0) || (self.car.frame.origin.x + self.step > self.roadMainView.frame.width + 1) {
            self.isCarOut = true
        } else {
            self.isCarOut = false
        }
    }
    
    //MARK: - Car actions
    
    func jumpCar() {
        self.itersectionTimer.invalidate()
        UIView.animate(withDuration: 0.3, animations: {
            self.car.frame = CGRect(x: self.car.frame.origin.x - 25, y: self.car.frame.origin.y - 25, width: self.car.bounds.width + 50, height: self.car.bounds.height + 50)
        }) { (_) in
            UIView.animate(withDuration: 0.3) {
                self.car.frame = CGRect(x: self.car.frame.origin.x + 25, y: self.car.frame.origin.y + 25, width: self.car.bounds.width - 50, height: self.car.bounds.height - 50)
                self.itersectionTimer.fire()
            }
        }
    }
    
    func moveLeft() {
        UIView.animate(withDuration: 0.2) {
            self.car.frame.origin.x -= self.step
        }
        self.checkOutRoadside()
        if self.isCarOut {
            self.explosionCar()
        }
    }
    
    func moveRight() {
        UIView.animate(withDuration: 0.2) {
            self.car.frame.origin.x += self.step
        }
        self.checkOutRoadside()
        if self.isCarOut {
            self.explosionCar()
        }
    }
    
    func explosionCar() {
        self.stopAnimations = true
        self.animateLinesTimer.invalidate()
        self.itersectionTimer.invalidate()
        self.animateRoadsideTimer.invalidate()
        let explosionImage = UIImageView()
        explosionImage.image = UIImage(named: "Boom")
        explosionImage.alpha = 0
        explosionImage.frame = CGRect(x: self.car.frame.origin.x, y: self.car.frame.origin.y, width: self.car.bounds.width, height: self.car.bounds.height)
        self.roadMainView.addSubview(explosionImage)
        UIView.animate(withDuration: 0.7, delay: 0, options: .curveEaseIn, animations: {
            explosionImage.alpha = 1
            self.car.alpha = 0
            self.block.alpha = 0
        }) { (_) in
            self.createGameOverScreen()
            self.car.removeFromSuperview()
            self.block.removeFromSuperview()
            explosionImage.removeFromSuperview()
        }
    }
    
    
}
