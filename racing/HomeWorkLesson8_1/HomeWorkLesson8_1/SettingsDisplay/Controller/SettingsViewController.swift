import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var mainViewLabel: UILabel!
    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var carModelLabel: UILabel!
    @IBOutlet weak var blockModelLabel: UILabel!
    @IBOutlet weak var difficultyLabel: UILabel!
    @IBOutlet weak var playerNameTextField: UITextField!
    @IBOutlet weak var carModelsCollectionVew: UICollectionView!
    @IBOutlet weak var blockModelsCollectionView: UICollectionView!
    @IBOutlet weak var difficultyCollectionView: UICollectionView!
    @IBOutlet weak var defaultsButton: CustomButton!
    @IBOutlet weak var saveButton: CustomButton!
    @IBOutlet weak var pickImageButton: UIButton!
    
    var carImages = [UIImage]()
    var blockImages = [UIImage]()
    var difficultyImages = [UIImage]()
    var currentPlayer = Settings()
    var playersSettings = [Settings]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainInit()
        if let data = StorageManager.shared.loadPlayersSettings(), let player = data.last {
            self.currentPlayer = player
        } else {
            self.currentPlayer.loadDefaultsSettings()
        }
        self.playerNameTextField.text = self.currentPlayer.name
        self.pickImageButton.setImage(self.currentPlayer.profileImage, for: .normal)
    }
    
    @IBAction func saveButtonPressed(_ sender: UIButton) {
        if let data = StorageManager.shared.loadPlayersSettings() {
            playersSettings = data
        } else {
            playersSettings = [Settings]()
        }
        self.checkPlayerName()
        self.currentPlayer.isNewPlayer = true
        self.playersSettings.append(self.currentPlayer)
        StorageManager.shared.savePlayersSettings(players: self.playersSettings)
        UserDefaults.standard.synchronize()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func defaultsButtonPressed() {
        self.currentPlayer.loadDefaultsSettings()
        self.playerNameTextField.text = self.currentPlayer.name
        self.pickImageButton.setImage(self.currentPlayer.profileImage, for: .normal)
    }
    
    @IBAction func pickImagePressed(_ sender: UIButton) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    func mainInit() {
        self.setupInfoCollectionView()
        self.setupCarModels()
        self.setupBlockModels()
        self.setupDifficultyModels()
        self.setupPickImageButton()
        self.setupMainViewLabels()
    }
    
    func setupPickImageButton() {
        self.pickImageButton.layer.cornerRadius = self.pickImageButton.frame.width / 2
        self.pickImageButton.layer.masksToBounds = true
        self.pickImageButton.layer.borderWidth = 3
        self.pickImageButton.layer.borderColor = UIColor.black.cgColor
    }
    
    func setupMainViewLabels() {
        self.mainViewLabel.layer.borderWidth = 3
        self.mainViewLabel.layer.borderColor = UIColor.black.cgColor
        self.mainViewLabel.layer.cornerRadius = 10
        self.mainViewLabel.layer.masksToBounds = true
        self.mainViewLabel.font = UIFont(name: "Korataki-LightItalic", size: 35)
        self.playerNameLabel.font = UIFont(name: "Korataki-LightItalic", size: 25)
        self.carModelLabel.font = UIFont(name: "Korataki-LightItalic", size: 18)
        self.blockModelLabel.font = UIFont(name: "Korataki-LightItalic", size: 18)
        self.difficultyLabel.font = UIFont(name: "Korataki-LightItalic", size: 16)
        self.defaultsButton.titleLabel?.font = UIFont(name: "Korataki-LightItalic", size: 18)
        self.saveButton.titleLabel?.font = UIFont(name: "Korataki-LightItalic", size: 18)
        self.defaultsButton.showsTouchWhenHighlighted = true
        self.saveButton.showsTouchWhenHighlighted = true
    }
    
    func setupInfoCollectionView() {
        carModelsCollectionVew.register(UINib(nibName: "SettingsCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "Cell")
        blockModelsCollectionView.register(UINib(nibName: "SettingsCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "Cell")
        difficultyCollectionView.register(UINib(nibName: "SettingsCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "Cell")
        carModelsCollectionVew.dataSource = self
        carModelsCollectionVew.delegate = self
        blockModelsCollectionView.dataSource = self
        blockModelsCollectionView.delegate = self
        difficultyCollectionView.dataSource = self
        difficultyCollectionView.delegate = self
    }
    
    func setupCarModels() {
        for item in Constants.carsIcons {
            if let icon = Manager.shared.getIconFor(item) {
                carImages.append(icon)
            }
        }
    }
    
    func setupBlockModels() {
        for item in Constants.blockIcons {
            if let icon = Manager.shared.getIconFor(item) {
                blockImages.append(icon)
            }
        }
    }
    
    func setupDifficultyModels() {
        for item in Constants.difficultyIcons {
            if let icon = Manager.shared.getIconFor(item) {
                difficultyImages.append(icon)
            }
        }
    }
    
    func checkPlayerName() {
        if self.playerNameTextField.text != "" {
            self.currentPlayer.name = self.playerNameTextField.text
        } else {
            showAlert(title: "Error", messaage: "Enter your name", style: .ok)
        }
    }
    
    
}

extension SettingsViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == carModelsCollectionVew {
            return carImages.count
        } else if collectionView == blockModelsCollectionView {
            return blockImages.count
        } else {
            return difficultyImages.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == carModelsCollectionVew {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! SettingsCollectionViewCell
            cell.imageView.image = carImages[indexPath.row]
            return cell
        } else if collectionView == blockModelsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! SettingsCollectionViewCell
            cell.imageView.image = blockImages[indexPath.row]
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! SettingsCollectionViewCell
            cell.imageView.image = difficultyImages[indexPath.row]
            return cell
        }
    }
    
}

extension SettingsViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == carModelsCollectionVew {
            guard let cell = carModelsCollectionVew.cellForItem(at: indexPath) else { return }
            cell.backgroundColor = .red
            self.currentPlayer.carModel = Constants.carsIcons[indexPath.row]
        } else if collectionView == blockModelsCollectionView {
            guard let cell = blockModelsCollectionView.cellForItem(at: indexPath) else { return }
            cell.backgroundColor = .red
            self.currentPlayer.blockModel = Constants.blockIcons[indexPath.row]
        } else {
            guard let cell = difficultyCollectionView.cellForItem(at: indexPath) else { return }
            cell.backgroundColor = .red
            self.currentPlayer.difficulty = Constants.difficultyIcons[indexPath.row]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == carModelsCollectionVew {
            guard let cell = carModelsCollectionVew.cellForItem(at: indexPath) else { return }
            cell.backgroundColor = .clear
        } else if collectionView == blockModelsCollectionView {
            guard let cell = blockModelsCollectionView.cellForItem(at: indexPath) else { return }
            cell.backgroundColor = .clear
        } else {
            guard let cell = difficultyCollectionView.cellForItem(at: indexPath) else { return }
            cell.backgroundColor = .clear
        }
    }
}

extension SettingsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let profileImage = info[.editedImage] as? UIImage else { return }
        self.pickImageButton.setImage(profileImage.withRenderingMode(.alwaysOriginal), for: .normal)
        self.currentPlayer.profileImage = profileImage
        self.dismiss(animated: true, completion: nil)
        
    }
}



