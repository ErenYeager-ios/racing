import UIKit

class RecordsViewController: UIViewController {
    
    @IBOutlet weak var tableOfRecords: UITableView!
    @IBOutlet weak var clearRecordsButton: CustomButton!
    
    var players = [Settings]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        self.clearRecordsButton.titleLabel?.font = UIFont(name: "Korataki-LightItalic", size: 18)
        self.clearRecordsButton.showsTouchWhenHighlighted = true
        guard let data = StorageManager.shared.loadPlayersSettings() else {return}
        self.players = data.filter {$0.isNewPlayer == false}
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(goMainMenu(_:)))
        self.tableOfRecords.addGestureRecognizer(tapRecognizer)
    }
    
    @IBAction func clearRecordsButtonPressed() {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
        self.players = []
        self.tableOfRecords.reloadData()
    }
    
    @IBAction func goMainMenu (_ sender: UITapGestureRecognizer) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func setupTableView() {
        self.tableOfRecords.register(UINib(nibName: "RecordsTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "Cell")
        self.tableOfRecords.backgroundColor = .black
        self.tableOfRecords.delegate = self
        self.tableOfRecords.dataSource = self
    }
    
}

extension RecordsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.players.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableOfRecords.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? RecordsTableViewCell else { return UITableViewCell()}
        cell.generateCell(player: self.players[indexPath.row])
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        guard let headerCell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? RecordsTableViewCell else {return UITableViewCell()}
        headerCell.frame.size.width = tableView.frame.width
        headerCell.backgroundColor = .darkGray
        headerView.addSubview(headerCell)
        return headerView
    }
    
}

extension RecordsViewController: UITableViewDelegate {
    
}
