import UIKit

class MainMenuViewController: UIViewController {
    
    @IBOutlet weak var startGame: CustomButton!
    
    @IBAction func startGamePressed(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(identifier: "GameViewController") as? GameViewController else { return }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func tableOfRecordsPressed(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(identifier: "RecordsViewController") as? RecordsViewController else { return }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func settingsPressed(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(identifier: "SettingsViewController") as? SettingsViewController else { return }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
}

