import Foundation

struct Constants {
    
    static let carsIcons = ["Car_1", "Car_2", "Car_3"]
    static let blockIcons = ["Roadblock_1", "Roadblock_2", "Roadblock_3"]
    static let difficultyIcons = ["Easy", "Normal", "Hard"]
    static let roadsideIcons = ["Tree_1", "Tree_2", "Tree_3"]
    static let key = "key"
}
