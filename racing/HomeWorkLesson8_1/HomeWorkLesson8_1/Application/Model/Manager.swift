import Foundation
import  UIKit

class Manager {
    static let shared = Manager()
    private init() {}
    
    func getIconFor(_ type: String) -> UIImage? {
        return UIImage(named: type)
    }
    
}
